import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorksListPageRoutingModule } from './works-list-routing.module';

import { WorksListPage } from './works-list.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorksListPageRoutingModule,
    ComponentsModule
  ],
  declarations: [WorksListPage]
})
export class WorksListPageModule {}
