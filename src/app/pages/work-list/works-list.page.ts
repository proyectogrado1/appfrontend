import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-works-list',
  templateUrl: './works-list.page.html',
  styleUrls: ['./works-list.page.scss'],
})
export class WorksListPage implements OnInit {

  works: any[];

  filter_per_hours: boolean = false;
  filter_category: string = "";

  constructor(private router: Router, private service: ApiService) { }

  ngOnInit() {
    this.getWorks();
  }

  ionViewWillEnter(){
    this.getWorks();
  }

  onFilterChange(){
    console.log(this.filter_category)
    console.log(this.filter_per_hours)
    this.getWorks();

  }

  getWorks(){
    let is_per_hours = null;
    if(this.filter_per_hours){
      is_per_hours = true;
    }

    let category = null;
    if(this.filter_category && this.filter_category.trim() != ""){
      category = this.filter_category.trim();
    }
    
    const request = this.service.getWorks(category, is_per_hours);
    request.subscribe(
      (res:any) => {
        this.works = res;
      },
      (err) => {
        console.log(err)
      }
    )
  }

  getStringDateCreation(created_at: number){
    //convert to hours
    const time = ( ( ( new Date().getTime() - created_at) / 1000 ) / 60 / 60);

    if(time > 24){
      return `Hace más de ${parseInt(time/24+"")} días`
    }
    else if(time > 1){
      return `Hace más de ${parseInt(time+"")} horas`
    }
    else{
      return `Hace menos de 1 hora`
    }
  }
  
  dashboardPage(){
    this.router.navigate(['dashboard'])
  }

  goToItemWork(_id: string){
    this.router.navigate(['work-item'], {queryParams: { _id } })
  }

  profilePage(){
    this.router.navigate(['profile'])
  }
}
