import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  error:string;
  registerForm: FormGroup;

  constructor(private router: Router, private formBuilder:FormBuilder, private service:ApiService, public alertController: AlertController) { 
  }

  loginPage() {
    this.router.navigate(['login'])
  }
  
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirm_password: ['', Validators.required],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      document: ['', Validators.required],
      document_type: ['CC-COL', Validators.required],
      country: ['Colombia', Validators.required],
      providence: ['Cundinamarca', Validators.required],
      city: ['Bogotá', Validators.required],
      neighborhood: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      type_register: ['', Validators.required],
    });
  }

  register(){
    if(this.registerForm.value.type_register == "worker"){
      const body = {
        "email": this.registerForm.value.email,
        "password": this.registerForm.value.password,
        "name": this.registerForm.value.name,
        "lastname": this.registerForm.value.lastname,
        "document": this.registerForm.value.document,
        "document_type": this.registerForm.value.document_type,
        "country": this.registerForm.value.country,
        "providence": this.registerForm.value.providence,
        "city": this.registerForm.value.city,
        "neighborhood": this.registerForm.value.neighborhood,
        "address":  this.registerForm.value.address,
        "phone": this.registerForm.value.phone
      }
      const request = this.service.workerRegister(body)
      request.subscribe(
        async (res:any) => {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Genial!',
            subHeader: 'Ya eres parte de la red',
            message: 'Registro Exitoso',
            buttons: ['Vale']
          });
      
          this.router.navigate(['login']);
          
          await alert.present();
        },
        (err) => {
          if(err.error.code == "email_used")
            this.error = "El correo ya ha sido usado"
          if(err.error.code == "document_used")
            this.error = "El documento de identidad ya ha sido usado"
          else
            this.error = "Ha ocurrido un error, por favor intente más tarde"
        }
      )
    }
    else{
      const body = {
        "email": this.registerForm.value.email,
        "password": this.registerForm.value.password,
        "name": this.registerForm.value.name,
        "lastname": this.registerForm.value.lastname,
        "document": this.registerForm.value.document,
        "document_type": this.registerForm.value.document_type,
        "country": this.registerForm.value.country,
        "providence": this.registerForm.value.providence,
        "city": this.registerForm.value.city,
        "neighborhood": this.registerForm.value.neighborhood,
        "address":  this.registerForm.value.address,
        "phone": this.registerForm.value.phone
      }
      const request = this.service.employerRegister(body)
      request.subscribe(
        async (res:any) => {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Genial!',
            subHeader: 'Ya eres parte de la red',
            message: 'Registro Exitoso',
            buttons: ['Vale']
          });
      
          this.router.navigate(['login']);
          
          await alert.present();
        },
        (err) => {
          if(err.error.code == "email_used")
            this.error = "El correo ya ha sido usado"
          if(err.error.code == "document_used")
            this.error = "El documento de identidad ya ha sido usado"
          else
            this.error = "Ha ocurrido un error, por favor intente más tarde"
        }
      )
    }
    
  }
}
