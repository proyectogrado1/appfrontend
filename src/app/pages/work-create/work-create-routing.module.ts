import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkCreatePage } from './work-create.page';

const routes: Routes = [
  {
    path: '',
    component: WorkCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkCreatePageRoutingModule {}
