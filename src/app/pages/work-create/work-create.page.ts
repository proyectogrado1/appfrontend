import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-work-create',
  templateUrl: './work-create.page.html',
  styleUrls: ['./work-create.page.scss'],
})
export class WorkCreatePage implements OnInit {

  form_create: FormGroup;

  constructor(private fb: FormBuilder, private service:ApiService, public alertController: AlertController,
     public loadingController: LoadingController,  private router: Router) { }

  ngOnInit() {
    this.form_create = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(100)]],
      is_per_hours: [false, [Validators.required]],
      price_per_hour: [0, [Validators.required]],
      price_total: [0, [Validators.required]],
      description: ['', [Validators.required]],
      is_visible: [true, [Validators.required]],
      status: ["created", [Validators.required]],
      country: ['CO', [Validators.required]],
      providence: ['', [Validators.required]],
      city: ['', [Validators.required]],
      neighborhood: ['', [Validators.required]],
      address: ['', [Validators.required]],
      category: ['', [Validators.required]],
    });
  }

  ionViewDidLeave(){
    this.form_create = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(100)]],
      is_per_hours: [false, [Validators.required]],
      price_per_hour: [0, [Validators.required]],
      price_total: [0, [Validators.required]],
      description: ['', [Validators.required]],
      is_visible: [true, [Validators.required]],
      status: ["created", [Validators.required]],
      country: ['CO', [Validators.required]],
      providence: ['', [Validators.required]],
      city: ['', [Validators.required]],
      neighborhood: ['', [Validators.required]],
      address: ['', [Validators.required]],
      category: ['', [Validators.required]],
    });
  }

  async submitForm(){
    if(this.form_create.valid){
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Guardando...',
      });
      await loading.present();

      const request = this.service.createWork(this.form_create.value);
      request.subscribe(
        async (response) => {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Creado',
            subHeader: 'Proyecto publicado',
            buttons: ['Aceptar']
          });
          this.router.navigate(['dashboard'])
          await alert.present();

          await loading.dismiss();
        },
        async (err) => {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Error',
            subHeader: 'Ha ocurrido un error, intente más tarde!',
            buttons: ['Aceptar']
          });
          await alert.present();

          await loading.dismiss();
        }
      )
    }
  }
}
