import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorksFinishedListPage } from './works-finished-list.page';

const routes: Routes = [
  {
    path: '',
    component: WorksFinishedListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorksFinishedListPageRoutingModule {}
