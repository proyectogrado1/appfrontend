import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorksCreatedListPageRoutingModule } from './work-created-list-routing.module';

import { WorksCreatedListPage } from './work-created-list.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorksCreatedListPageRoutingModule,
    ComponentsModule
  ],
  declarations: [WorksCreatedListPage]
})
export class WorksCreatedListPageModule {}
