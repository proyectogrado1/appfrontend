import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorksCreatedListPage } from './work-created-list.page';

const routes: Routes = [
  {
    path: '',
    component: WorksCreatedListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorksCreatedListPageRoutingModule {}
