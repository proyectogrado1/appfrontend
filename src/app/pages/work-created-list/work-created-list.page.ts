import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-work-created-list',
  templateUrl: './work-created-list.page.html',
  styleUrls: ['./work-created-list.page.scss'],
})
export class WorksCreatedListPage implements OnInit {

  works_created: any[];

  constructor(private router: Router, private service: ApiService) { }

  ngOnInit() {
    this.getCreatedWorks();
  }

  ionViewWillEnter(){
    this.getCreatedWorks();
  }

  getCreatedWorks(){
    const request = this.service.getCreatedWorks();
    request.subscribe(
      (res:any) => {
        this.works_created = res;
        console.log(res)
      },
      (err) => {
        console.log(err)
      }
    )
  }

  getStringDateCreation(created_at: number){
    //convert to hours
    const time = ( ( ( new Date().getTime() - created_at) / 1000 ) / 60 / 60);

    if(time > 24){
      return `Creado hace más de ${parseInt(time/24+"")} días`
    }
    else if(time > 1){
      return `Creado hace más de ${parseInt(time+"")} horas`
    }
    else{
      return `Creado hace menos de 1 hora`
    }
  }
  
  dashboardPage(){
    this.router.navigate(['dashboard'])
  }

  goToItemWork(_id: string){
    this.router.navigate(['work-item'], {queryParams: { _id } })
  }

  getCountApplicants(work:any){
    return work.applicants?.length || 0;
  }
  
  profilePage(){
    this.router.navigate(['profile'])
  }
}
