import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorksAppliedListPage } from './works-applied-list.page';

const routes: Routes = [
  {
    path: '',
    component: WorksAppliedListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorksListPageRoutingModule {}
