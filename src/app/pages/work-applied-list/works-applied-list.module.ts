import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorksListPageRoutingModule } from './works-applied-list-routing.module';

import { WorksAppliedListPage } from './works-applied-list.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorksListPageRoutingModule,
    ComponentsModule
  ],
  declarations: [WorksAppliedListPage]
})
export class WorksAppliedListPageModule {}
