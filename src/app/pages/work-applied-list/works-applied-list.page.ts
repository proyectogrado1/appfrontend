import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-works-applied-list',
  templateUrl: './works-applied-list.page.html',
  styleUrls: ['./works-applied-list.page.scss'],
})
export class WorksAppliedListPage implements OnInit {

  works_applied: any[];

  constructor(private router: Router, private service: ApiService) { }

  ngOnInit() {
    this.getWorksApplied();
  }

  ionViewWillEnter(){
    this.getWorksApplied();
  }

  getWorksApplied(){
    const request = this.service.getWorksApplied();
    request.subscribe(
      (res:any) => {
        this.works_applied = res.filter((work) => {
          return work.work_data.status != "finished"
        });
        console.log(res)
      },
      (err) => {
        console.log(err)
      }
    )
  }

  getStringDateCreation(created_at: number){
    //convert to hours
    const time = ( ( ( new Date().getTime() - created_at) / 1000 ) / 60 / 60);

    if(time > 24){
      return `Aplicado hace más de ${parseInt(time/24+"")} días`
    }
    else if(time > 1){
      return `Aplicado hace más de ${parseInt(time+"")} horas`
    }
    else{
      return `Aplicado hace menos de 1 hora`
    }
  }
  
  dashboardPage(){
    this.router.navigate(['dashboard'])
  }

  goToItemWork(_id: string){
    this.router.navigate(['work-item'], {queryParams: { _id } })
  }

  profilePage(){
    this.router.navigate(['profile'])
  }
}
