import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Filler,
  Legend,
  Title,
  Tooltip
} from 'chart.js';
import { LoadingController } from '@ionic/angular';

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Filler,
  Legend,
  Title,
  Tooltip
);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  @ViewChild("barCanvas", { static: true }) barCanvas;

  private barChart: Chart;
  public menustatus: any;

  public user;
  public balance;
  public works_currently_employer_count;
  public user_qualification;

  public works_currently_employer;
  public works_currently_worker;

  myInterval;


  constructor(private router: Router, private service: ApiService, public loadingController: LoadingController) { }

  async getBalance(){
    this.user = JSON.parse(localStorage.getItem('user'))

    console.log(this.user)

    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    await loading.present();

    const request = this.service.getBalance();
    request.subscribe(
      async (res) => {
        this.balance = res;
        await loading.dismiss();
      },
      async (err) => {
        await loading.dismiss();
      }
    )

    if(this.user.is_worker){
      await loading.present();

      const request_working1 = this.service.getCurrentlyWorkingList();

      request_working1.subscribe(
        async (res_working) => {
          this.works_currently_worker = res_working
          await loading.dismiss();

          const request_working3 = this.service.getQualification();
          request_working3.subscribe(
            (res)=> {
              this.user_qualification = res;
            }
          )

        },
        async (err) => {
          await loading.dismiss();
        }
      )
    }

    if(this.user.is_employeer){
      await loading.present();

      const request_working2 = this.service.getWorksInProgress();
      request_working2.subscribe(
        async (res_working:any) => {
          this.works_currently_employer = res_working
          this.works_currently_employer_count = res_working.length;
          await loading.dismiss();
        },
        async (err) => {
          await loading.dismiss();
        }
      )
    }
  }

  convertInt(number){
    return parseInt(number)
  }

  menuopen() {
    alert("function changecolor");
    this.menustatus = 'open';
  }

  notificationsPage() {
    this.router.navigate(['notifications'])
  }

  profilePage() {
    this.router.navigate(['profile'])
  }

  async ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'))
    this.initBarChar();
    await this.getBalance();
  }

  ngOnDestroy(){
  }

  async updateInfo(){
    await this.getBalance();
  }
  
  initBarChar(){
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: [" ", "1week", "2week", "3week", "4week", "5week", " "],
        datasets: [{
          borderWidth: 2,
          borderColor: "#28A745",
          pointBorderWidth: 0,
          pointHoverRadius: 0,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 0,
          pointRadius: 0,
          pointHitRadius: 0,
          data: [65, 50, 80, 90, 56, 85, 50],
          fill: false
        },
        {

          borderColor: "rgba(255,255,255,0.5)",
          borderWidth: 1,
          borderDash: [3, 5],
          pointBorderWidth: 0,
          pointHoverRadius: 0,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 0,
          pointRadius: 0,
          pointHitRadius: 0,
          data: [40, 60, 70, 64, 89, 50, 69],
          fill: false
        }

        ]
      },
      options: {
        responsive: true,
        animations: {
          tension: {
            duration: 1000,
            easing: 'linear',
            from: 1,
            to: 0,

          }
        },
        plugins: {
          legend: {
            display: false
          },
          tooltip: {
            enabled: false,
          }
        },
        scales: {
          x: {

            grid:
            {
              drawBorder: false,
              display: false
            }

          },
          y: {
            display: false
          }
        }
      }
    });
  }

  goToWorksListPage() {
    this.router.navigate(['works-list']);
  }

  goToWorksAppliedListPage() {
    this.router.navigate(['works-applied-list']);
  }

  goToWorkCreatePage() {
    this.router.navigate(['work-create']);
  }

  goToCreatedWorkListPage() {
    this.router.navigate(['work-created-list']);
  }

  goToItemWork(_id: string){
    this.router.navigate(['work-item'], {queryParams: { _id } })
  }

  goToFinishedWorks(){
    this.router.navigate(['work-finished-list'])
  }
}
