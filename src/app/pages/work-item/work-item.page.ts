import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-work-item',
  templateUrl: './work-item.page.html',
  styleUrls: ['./work-item.page.scss'],
})
export class WorkItemPage implements OnInit {

  current_user: any;

  qualification: number = 1;

  _id: string;
  work:any;
  applied: boolean;
  selected: any;

  applicants: any;
  workers_selected: any;

  myInterval;

  constructor(private router: Router, private service: ApiService, private route: ActivatedRoute,
    public alertController: AlertController, public loadingController: LoadingController) { }

  goToWorkList() {
    this.router.navigate(['works-list'])
  }

  ngOnInit() {
    this.current_user = JSON.parse(localStorage.getItem('user'));
    this._id = this.route.snapshot.queryParams._id;
    this.getWork();
    this.myInterval = setInterval( async () => { this.getWork() }, 10000);    
    console.log(this._id)
  }

  ngOnDestroy(){
    clearInterval(this.myInterval)
  }
  
  async getWork(){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    
    await loading.present();
    const request_work = this.service.getWork(this._id);
    request_work.subscribe(
      async (res) => {
        this.work = res;
        if(this.current_user.is_worker){
          const request_check = this.service.checkWorkerAppliedToWork(this._id);
          request_check.subscribe(
            async (res: any) => {
              this.applied = res.check;
              this.selected = res.selected;
              await loading.dismiss();
            },
            async (err) => {
              const alert = await this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Error',
                subHeader: 'Ha ocurrido un error, intente más tarde!',
                buttons: ['Aceptar']
              });
              await alert.present();
              await loading.dismiss();
            }
          )
        }
        else{
          await loading.dismiss();
        }
      },
      async (err) => {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          subHeader: 'Ha ocurrido un error, intente más tarde!',
          buttons: ['Aceptar']
        });
        await alert.present();
        await loading.dismiss();
      }
    )

    if(this.current_user.is_employeer){
      await loading.present();
      const request_workers_applies = this.service.getWorkersAndApplies(this._id);
      request_workers_applies.subscribe(
        async (res:any) => {
          this.applicants = res?.applicants || [];
          this.workers_selected = res?.workers_selected || [];
          await loading.dismiss();
        },
        async (err) => {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Error',
            subHeader: 'Ha ocurrido un error, intente más tarde!',
            buttons: ['Aceptar']
          });
          await alert.present();
          await loading.dismiss();
        }
      )

    }else{
      console.log(this.work)
      await loading.dismiss();
    } 
  }

  async applyToWork(event:any){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    await loading.present();

    this.goToWorkList();

    const request = this.service.workerApplyToWork(this._id, event?.comments);
    request.subscribe(
      async (res) => {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Confirmación',
          subHeader: 'Aplicación exitosa',
          buttons: ['Aceptar']
        });
    
        await alert.present();
        await loading.dismiss();
      },
      async (err) => {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          subHeader: 'Ha ocurrido un error, intente más tarde!',
          buttons: ['Aceptar']
        });
        await alert.present();

        await loading.dismiss();
      }
    )
  }


  async confirmApplyToWork() {
    const alert = await this.alertController.create({
      cssClass: 'confirm-alert',
      header: 'Confirmar aplicación!',
      inputs: [
        {
          name: 'comments',
          type: 'textarea',
          placeholder: 'Describa aquí información sobre usted, sus experiencias relacionadas con este trabajo y como piensa ejecutarlo'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Aplicar',
          handler: (e) => {
            this.applyToWork(e)
          }
        }
      ]
    });

    await alert.present();
  }

  async contratar(id_worker){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    await loading.present();

    const request_workers_applies = this.service.selectWorkerToWork(this.work._id, id_worker);
    request_workers_applies.subscribe(
      async (res:any) => {
        this.applicants = res?.applicants || [];
        this.workers_selected = res?.workers_selected || [];

        await loading.dismiss();

        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Trabajador Seleccionado',
          subHeader: 'El trabajador se contactará con usted para dar inicio al trabajo.',
          buttons: ['Aceptar']
        });
        await alert.present();
        this.getWork();
        
      },
      async (err) => {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          subHeader: 'Ha ocurrido un error, intente más tarde!',
          buttons: ['Aceptar']
        });
        await alert.present();
        await loading.dismiss();
      }
    )
  }

  async endWork(){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
  
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Desea terminar el trabajo?',
      message: 'Al aceptar los fondos se acreditarán directamente al trabajador',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Aceptar',
          handler: async () => {

            if(this.work.is_per_hours){

              console.log("por horas")

              const alert2 = await this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Confirme las horas realizadas por el trabajador',
                inputs: [
                  {
                    name: 'horas',
                    type: 'number',
                    placeholder: 'Horas'
                  }
                ],
                buttons: [
                  {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                      console.log('Confirm Cancel');
                    }
                  }, {
                    text: 'Aceptar',
                    handler: async (dataForm) => {

                      await loading.present();
                      const request = this.service.endWork(this.work._id, this.work.price_per_hour*parseInt(dataForm.horas))
                      request.subscribe(
                        async (response) => {
                          const alert = await this.alertController.create({
                            cssClass: 'my-custom-class',
                            header: 'Trabajo Terminado',
                            subHeader: 'Los fondos se depositaron al trabajador.',
                            buttons: ['Aceptar']
                          });
                          await loading.dismiss();
                          await alert.present();
                          await this.getWork();
                        },
                        async (error) => {
                          const alert = await this.alertController.create({
                            cssClass: 'my-custom-class',
                            header: 'Error',
                            subHeader: 'Ha ocurrido un error, intente más tarde!',
                            buttons: ['Aceptar']
                          });
                          await alert.present();
                          await loading.dismiss();
                        }
                      )

                    }
                  }
                ]
              });
          
              await alert2.present();
            }


            else{

              await loading.present();
              const request = this.service.endWork(this.work._id, this.work.price_total)
              request.subscribe(
                async (response) => {
                  const alert = await this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: 'Trabajo Terminado',
                    subHeader: 'Los fondos se depositaron al trabajador.',
                    buttons: ['Aceptar']
                  });
                  await loading.dismiss();
                  await alert.present();
                  await this.getWork();
                },
                async (error) => {
                  const alert = await this.alertController.create({
                    cssClass: 'my-custom-class',
                    header: 'Error',
                    subHeader: 'Ha ocurrido un error, intente más tarde!',
                    buttons: ['Aceptar']
                  });
                  await alert.present();
                  await loading.dismiss();
                }
              )
            }
          }
        }
      ]
    });

    await alert.present();
  }


  async changeCalification(event){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    await loading.present();

    const request = this.service.qualificateWorker(this.work._id, this.qualification);
    request.subscribe(
      async (res)=> {
        await this.getWork();
        loading.dismiss();
      },
      async (err)=> {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          subHeader: 'Ha ocurrido un error, intente más tarde!',
          buttons: ['Aceptar']
        });
        await alert.present();
        await loading.dismiss();
      }
    )

  }

  convertInt(number){
    return parseInt(number)
  }
}
