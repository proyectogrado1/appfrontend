import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkItemPage } from './work-item.page';

const routes: Routes = [
  {
    path: '',
    component: WorkItemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkItemPageRoutingModule {}
