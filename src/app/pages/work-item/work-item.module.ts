import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkItemPageRoutingModule } from './work-item-routing.module';

import { WorkItemPage } from './work-item.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkItemPageRoutingModule,
    ComponentsModule
  ],
  declarations: [WorkItemPage]
})
export class WorkItemPageModule {}
