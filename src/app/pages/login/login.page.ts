import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  error: string = null;

  email: string = "";
  password: string = "";

  constructor(private router: Router, private service: ApiService, public loadingController: LoadingController) { }

  registerPage() {
    this.router.navigate(['register'])
  }

  forgotpasswordPage() {
    this.router.navigate(['forgotpassword'])
  }

  async login() {
    localStorage.clear();
    const request = this.service.login(this.email, this.password);
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
    });
    await loading.present();
    request.subscribe(
      async (res:any) => {
        localStorage.setItem('token',res.token)
        localStorage.setItem('user', JSON.stringify(res.user))
        this.router.navigate(['dashboard'])
        await loading.dismiss();
      },
      async (err) => {
        if(err.error.code == "user_or_password_invalid")
          this.error = "El usuario o la contraseña son inválidos"
        else
          this.error = "Ha ocurrido un error, por favor intente más tarde"
          await loading.dismiss();
      }
    )
  }

  ngOnInit() {
  }

}
