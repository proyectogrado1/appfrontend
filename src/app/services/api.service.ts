import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  URL_API: string = 'https://webservicepg.herokuapp.com'
  //URL_API: string = 'http://localhost:8080'

  constructor(private http: HttpClient) { }

  public login(email:string, password:string): Observable<HttpResponse<any>> {
    const request = this.http.post<any>(this.URL_API+"/v1/login", {email: email, password: password});
    return request;
  }

  public getBalance(): Observable<HttpResponse<any>> {
    const request = this.http.get<any>(
      this.URL_API+"/v1/user/balance", 
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')}}
    );
    return request;
  }

  public getQualification(): Observable<HttpResponse<any>> {
    const request = this.http.get<any>(
      this.URL_API+"/v1/user/qualification", 
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')}}
    );
    return request;
  }

  public workerRegister(worker:any): Observable<HttpResponse<any>> {
    const request = this.http.post<any>(this.URL_API+"/v1/worker/register", worker, {});
    return request;
  }

  public employerRegister(employer:any): Observable<HttpResponse<any>> {
    const request = this.http.post<any>(this.URL_API+"/v1/employee/register", employer, {});
    return request;
  }

  public getWorks(category:string, is_per_hours:boolean): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/list?";

    if(category)
      url += "&category="+category

    if(is_per_hours)
      url += "&is_per_hours="+String(is_per_hours)

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getWork(_id_work: string): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/"+_id_work;

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public workerApplyToWork(_id_work: string, comments:string ): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/"+_id_work+"/worker/apply";

    const request = this.http.post<any>(
      url,
      { comments },
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public checkWorkerAppliedToWork(_id_work: string): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/"+_id_work+"/worker/check";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getWorksApplied(): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/applied";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public createWork(work:any): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/create";

    const request = this.http.put<any>(
      url,
      work,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getCreatedWorks(): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/created";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getWorkersAndApplies(_id_work: string): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/"+_id_work+"/workers";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public selectWorkerToWork(_id_work: string, _id_worker:string ): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/"+_id_work+"/select";

    const request = this.http.post<any>(
      url,
      { id_worker: _id_worker },
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getCurrentlyWorkingList(): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/worker/working";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getWorksInProgress(): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/employer/inprogress";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public endWork(_id_work: string, amount:number ): Observable<HttpResponse<any>> {
    console.log(_id_work)
    let url = this.URL_API+"/v1/work/"+_id_work+"/finish";

    const request = this.http.post<any>(
      url,
      { amount },
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public getWorksFinishedByWorker(): Observable<HttpResponse<any>> {
    let url = this.URL_API+"/v1/work/worker/finish";

    const request = this.http.get<any>(
      url,
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }

  public qualificateWorker(_id_work: string, qualification:number ): Observable<HttpResponse<any>> {
    console.log(_id_work)
    let url = this.URL_API+"/v1/work/"+_id_work+"/worker/qualification";

    const request = this.http.post<any>(
      url,
      { qualification },
      { headers: { Authorization: "Bearer "+localStorage.getItem('token')} }
    );
    return request;
  }
}
